package com.anacoimbra.concrete

import com.anacoimbra.concrete.api.Service
import com.anacoimbra.concrete.model.Repositories
import io.reactivex.observers.TestObserver
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun getRepositories_isSuccess() {
        val subscriber = TestObserver.create<Repositories>()
        Service.call()
                .getRepositories("language:Java", "stars", 1)
                .subscribe(subscriber)
        subscriber.assertNoErrors()
        subscriber.assertComplete()
    }
}
