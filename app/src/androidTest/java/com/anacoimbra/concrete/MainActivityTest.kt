package com.anacoimbra.concrete

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.anacoimbra.concrete.presenter.MainPresenter
import com.anacoimbra.concrete.view.activity.MainActivity
import com.anacoimbra.concrete.view.activity.PullRequestActivity
import com.anacoimbra.concrete.view.adapter.RepositoryViewHolder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by aninh on 02/19/2018.
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @JvmField
    @Rule
    var activityRule = ActivityTestRule(MainActivity::class.java)

    private lateinit var presenter: MainPresenter

    @Before
    fun setup() {
        presenter = MainPresenter()
        activityRule.activity.presenter = presenter
    }

    @Test
    fun testRecyclerView() {
        val recyclerView = onView(withId(R.id.repositories))
        recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition<RepositoryViewHolder>(0, click()))
        intended(hasComponent(PullRequestActivity::class.java.name))
    }

}