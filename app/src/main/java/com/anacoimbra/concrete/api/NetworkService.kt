package com.anacoimbra.concrete.api

import com.anacoimbra.concrete.model.PullRequest
import com.anacoimbra.concrete.model.Repositories
import com.anacoimbra.concrete.model.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by aninh on 02/16/2018.
 */
interface NetworkService {

    @GET("search/repositories")
    fun getRepositories(@Query("q") query: String, @Query("sort") sort: String,
                        @Query("page") page: Int) : Observable<Repositories>

    @GET("repos/{user}/{repo}/pulls")
    fun getPullRequests(@Path("user") username: String,
                        @Path("repo") repository: String) : Observable<ArrayList<PullRequest>>

}