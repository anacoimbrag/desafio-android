package com.anacoimbra.concrete.inject

import android.app.Application
import com.anacoimbra.concrete.view.activity.MainActivity
import com.anacoimbra.concrete.app.App
import com.anacoimbra.concrete.view.activity.PullRequestActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by aninh on 07/02/2018.
 */
@Module(includes = [(AndroidSupportInjectionModule::class)])
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun application(app: App) : Application

    @PerActivity
    @ContributesAndroidInjector
    abstract fun mainActivityInjector() : MainActivity

    @PerActivity
    @ContributesAndroidInjector
    abstract fun pullRequestActivityInjector() : PullRequestActivity

}