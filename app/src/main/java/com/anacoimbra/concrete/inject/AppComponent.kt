package com.anacoimbra.concrete.inject

import com.anacoimbra.concrete.app.App
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by aninh on 07/02/2018.
 */
@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}