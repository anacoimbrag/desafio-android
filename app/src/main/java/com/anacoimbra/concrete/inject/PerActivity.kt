package com.anacoimbra.concrete.inject

import javax.inject.Scope

/**
 * Created by aninh on 07/02/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity