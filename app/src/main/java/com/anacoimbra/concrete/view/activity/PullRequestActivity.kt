package com.anacoimbra.concrete.view.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.anacoimbra.concrete.R
import com.anacoimbra.concrete.app.EXTRA_REPOSITORY
import com.anacoimbra.concrete.contract.PullRequestContract
import com.anacoimbra.concrete.model.Item
import com.anacoimbra.concrete.model.PullRequest
import com.anacoimbra.concrete.presenter.PullRequestPresenter
import com.anacoimbra.concrete.view.adapter.PullRequestAdapter
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_pull_request.*
import javax.inject.Inject

class PullRequestActivity : DaggerAppCompatActivity(), PullRequestContract.View {

    @Inject
    lateinit var presenter: PullRequestPresenter

    private var adapter = PullRequestAdapter()

    private val maxItemPerRequest = 30
    private var page = 1

    private lateinit var repository: Item

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        repository = intent.getSerializableExtra(EXTRA_REPOSITORY) as Item
        title = repository.name

        presenter.attachView(this)

        pull_requests.adapter = adapter
        pull_requests.addOnScrollListener(createInfiniteScrollListener())
    }

    override fun onResume() {
        super.onResume()

        presenter.getPullRequests(repository.owner.login, repository.name)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun loading() {
        progress.visibility = View.VISIBLE
    }

    override fun onError(error: Throwable) {
        progress.visibility = View.GONE
    }

    override fun onGetPullRequests(pullRequests: ArrayList<PullRequest>, opened: Int, closed: Int) {
        progress.visibility = View.GONE
        numbers.text = getString(R.string.numbers_placeholder).format(opened, closed)
        if (adapter.pullRequests != null && adapter.pullRequests!!.isNotEmpty()) {
            adapter.pullRequests?.addAll(pullRequests)
        }
        else adapter.pullRequests = pullRequests
    }

    private fun createInfiniteScrollListener() : InfiniteScrollListener {
        return object : InfiniteScrollListener(maxItemPerRequest, pull_requests.layoutManager as LinearLayoutManager) {
            override fun onScrolledToEnd(firstVisibleItemPosition: Int) {
                page ++
                presenter.getPullRequests(repository.name, repository.owner.login)
                refreshView(pull_requests, adapter, firstVisibleItemPosition)
            }
        }
    }
}
