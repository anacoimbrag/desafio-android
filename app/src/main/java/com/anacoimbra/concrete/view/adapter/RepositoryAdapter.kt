package com.anacoimbra.concrete.view.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anacoimbra.concrete.R
import com.anacoimbra.concrete.app.EXTRA_REPOSITORY
import com.anacoimbra.concrete.model.Item
import com.anacoimbra.concrete.util.RoundedTransformation
import com.anacoimbra.concrete.view.activity.PullRequestActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.repository_item.view.*

/**
 * Created by aninh on 02/16/2018.
 */
class RepositoryAdapter : RecyclerView.Adapter<RepositoryViewHolder>() {

    var repositories: ArrayList<Item>? = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.repository_item, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (repositories != null) repositories!!.size else 0
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        holder?.bind(repositories?.get(position))
    }

}

class RepositoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(repository: Item?) = with(itemView) {
        repository_name.text = repository?.name
        repository_desciption.text = repository?.description
        forks_number.text = repository?.forksCount.toString()
        stars_number.text = repository?.stargazersCount.toString()
        username.text = repository?.owner?.login

        Picasso.with(context)
                .load(repository?.owner?.avatarUrl)
                .transform(RoundedTransformation(5, 1))
                .into(user_picture)

        container.setOnClickListener {
            openActivity(repository)
        }
    }

    fun openActivity(item: Item?) {
        val context = itemView.context
        val intent = Intent(context, PullRequestActivity::class.java)
        intent.putExtra(EXTRA_REPOSITORY, item)
        context.startActivity(intent)
    }
}