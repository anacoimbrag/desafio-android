package com.anacoimbra.concrete.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anacoimbra.concrete.R
import com.anacoimbra.concrete.model.PullRequest
import com.anacoimbra.concrete.util.RoundedTransformation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.pull_request_item.view.*

/**
 * Created by aninh on 02/16/2018.
 */
class PullRequestAdapter : RecyclerView.Adapter<PullRequestViewHolder>() {

    var pullRequests: ArrayList<PullRequest>? = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullRequestViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.pull_request_item, parent, false)
        return PullRequestViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (pullRequests != null) pullRequests!!.size else 0
    }

    override fun onBindViewHolder(holder: PullRequestViewHolder?, position: Int) {
        holder?.bind(pullRequests?.get(position))
    }

}

class PullRequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(pullRequest: PullRequest?) = with(itemView) {
        pull_request_name.text = pullRequest?.title
        pull_request_description.text = pullRequest?.body
        username.text = pullRequest?.user?.login

        Picasso.with(context)
                .load(pullRequest?.user?.avatarUrl)
                .transform(RoundedTransformation(5, 1))
                .into(user_picture)
    }
}