package com.anacoimbra.concrete.view.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.anacoimbra.concrete.R
import com.anacoimbra.concrete.contract.MainContract
import com.anacoimbra.concrete.model.Repositories
import com.anacoimbra.concrete.presenter.MainPresenter
import com.anacoimbra.concrete.view.adapter.RepositoryAdapter
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainPresenter

    private var adapter = RepositoryAdapter()

    private val maxItemPerRequest = 30
    private var page = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.attachView(this)

        repositories.adapter = adapter
        repositories.addOnScrollListener(createInfiniteScrollListener())

        presenter.getRepositories(page)
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("position", repositories.verticalScrollbarPosition) // get current recycle view position here.
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        repositories.scrollToPosition(savedInstanceState?.getInt("position", 0)!!)
    }

    override fun loading() {
        progress.visibility = View.VISIBLE
    }

    override fun onError(error: Throwable) {
        progress.visibility = View.GONE
    }

    override fun onGetRepositories(repositories: Repositories) {
        progress.visibility = View.GONE
        if (adapter.repositories != null && adapter.repositories!!.isNotEmpty()) {
            adapter.repositories?.addAll(repositories.items.orEmpty())
        }
        else adapter.repositories = repositories.items
    }

    private fun createInfiniteScrollListener() : InfiniteScrollListener {
        return object : InfiniteScrollListener(maxItemPerRequest, repositories.layoutManager as LinearLayoutManager) {
            override fun onScrolledToEnd(firstVisibleItemPosition: Int) {
                page ++
                presenter.getRepositories(page)
                refreshView(repositories, adapter, firstVisibleItemPosition)
            }
        }
    }
}
