package com.anacoimbra.concrete.app

import com.anacoimbra.concrete.inject.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Created by aninh on 02/16/2018.
 */
class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> = DaggerAppComponent.builder().create(this)
}