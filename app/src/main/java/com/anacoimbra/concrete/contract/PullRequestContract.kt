package com.anacoimbra.concrete.contract

import com.anacoimbra.concrete.model.PullRequest

/**
 * Created by aninh on 02/16/2018.
 */
class PullRequestContract {
    interface View {
        fun loading()
        fun onError(error: Throwable)
        fun onGetPullRequests(pullRequests: ArrayList<PullRequest>, opened: Int, closed: Int)
    }

    interface Presenter {
        fun attachView(view: View)
        fun getPullRequests(user: String, repo: String)
    }
}