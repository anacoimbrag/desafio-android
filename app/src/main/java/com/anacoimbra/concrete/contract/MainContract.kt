package com.anacoimbra.concrete.contract

import com.anacoimbra.concrete.model.Repositories

/**
 * Created by aninh on 02/16/2018.
 */
class MainContract {

    interface View {
        fun loading()
        fun onError(error: Throwable)
        fun onGetRepositories(repositories: Repositories)
    }

    interface Presenter {
        fun attachView(view: View)
        fun getRepositories(page: Int)
    }
}