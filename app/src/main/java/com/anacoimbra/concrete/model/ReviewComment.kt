package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class ReviewComment(@SerializedName("href")
                         val href: String = "")