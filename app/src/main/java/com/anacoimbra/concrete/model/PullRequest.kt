package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class PullRequest(@SerializedName("issue_url")
                       val issueUrl: String = "",
                       @SerializedName("_links")
                       val Links: Links,
                       @SerializedName("diff_url")
                       val diffUrl: String = "",
                       @SerializedName("created_at")
                       val createdAt: String = "",
                       @SerializedName("title")
                       val title: String = "",
                       @SerializedName("body")
                       val body: String = "",
                       @SerializedName("head")
                       val head: Head,
                       @SerializedName("author_association")
                       val authorAssociation: String = "",
                       @SerializedName("number")
                       val number: Int = 0,
                       @SerializedName("patch_url")
                       val patchUrl: String = "",
                       @SerializedName("updated_at")
                       val updatedAt: String = "",
                       @SerializedName("merge_commit_sha")
                       val mergeCommitSha: String = "",
                       @SerializedName("comments_url")
                       val commentsUrl: String = "",
                       @SerializedName("review_comment_url")
                       val reviewCommentUrl: String = "",
                       @SerializedName("id")
                       val id: Int = 0,
                       @SerializedName("state")
                       val state: String = "",
                       @SerializedName("locked")
                       val locked: Boolean = false,
                       @SerializedName("commits_url")
                       val commitsUrl: String = "",
                       @SerializedName("closed_at")
                       val closedAt: String? = null,
                       @SerializedName("statuses_url")
                       val statusesUrl: String = "",
                       @SerializedName("merged_at")
                       val mergedAt: String? = null,
                       @SerializedName("url")
                       val url: String = "",
                       @SerializedName("labels")
                       val labels: List<LabelsItem>?,
                       @SerializedName("milestone")
                       val milestone: Milestone,
                       @SerializedName("html_url")
                       val htmlUrl: String = "",
                       @SerializedName("review_comments_url")
                       val reviewCommentsUrl: String = "",
                       @SerializedName("assignee")
                       val assignee: String? = null,
                       @SerializedName("user")
                       val user: User,
                       @SerializedName("base")
                       val base: Base)