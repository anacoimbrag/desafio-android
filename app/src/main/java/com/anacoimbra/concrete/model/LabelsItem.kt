package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class LabelsItem(@SerializedName("default")
                      val default: Boolean = false,
                      @SerializedName("color")
                      val color: String = "",
                      @SerializedName("name")
                      val name: String = "",
                      @SerializedName("id")
                      val id: Int = 0,
                      @SerializedName("url")
                      val url: String = "")