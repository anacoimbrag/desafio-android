package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class ReviewComments(@SerializedName("href")
                          val href: String = "")