package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class Comments(@SerializedName("href")
                    val href: String = "")