package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class License(@SerializedName("name")
                   val name: String = "",
                   @SerializedName("spdx_id")
                   val spdxId: String = "",
                   @SerializedName("key")
                   val key: String = "",
                   @SerializedName("url")
                   val url: String = "") : Serializable