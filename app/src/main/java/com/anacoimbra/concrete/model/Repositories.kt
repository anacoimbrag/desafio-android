package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Repositories(@SerializedName("total_count")
                        val totalCount: Int = 0,
                        @SerializedName("incomplete_results")
                        val incompleteResults: Boolean = false,
                        @SerializedName("items")
                        val items: ArrayList<Item>?) : Serializable