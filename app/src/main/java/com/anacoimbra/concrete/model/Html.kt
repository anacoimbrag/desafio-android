package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class Html(@SerializedName("href")
                val href: String = "")