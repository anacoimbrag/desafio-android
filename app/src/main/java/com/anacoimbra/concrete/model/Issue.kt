package com.anacoimbra.concrete.model

import com.google.gson.annotations.SerializedName

data class Issue(@SerializedName("href")
                 val href: String = "")