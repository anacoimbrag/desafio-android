package com.anacoimbra.concrete.presenter

import com.anacoimbra.concrete.api.Service
import com.anacoimbra.concrete.contract.MainContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by aninh on 02/16/2018.
 */
class MainPresenter @Inject constructor() : MainContract.Presenter {

    private var view: MainContract.View? = null
    private val subscriptions = CompositeDisposable()

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun getRepositories(page: Int) {
        subscriptions.add(
                Service.call()
                        .getRepositories("language:Java", "stars", page)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                { repositories -> view?.onGetRepositories(repositories) },
                                { error -> view?.onError(error) }
                        )
        )
    }

    fun stop() {
        subscriptions.clear()
    }

}