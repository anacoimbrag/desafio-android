package com.anacoimbra.concrete.presenter

import com.anacoimbra.concrete.api.Service
import com.anacoimbra.concrete.contract.PullRequestContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by aninh on 02/16/2018.
 */
class PullRequestPresenter @Inject constructor() : PullRequestContract.Presenter {

    private var view: PullRequestContract.View? = null
    private val subscriptions = CompositeDisposable()

    override fun attachView(view: PullRequestContract.View) {
        this.view = view
    }

    override fun getPullRequests(user: String, repo: String) {
        var opened = 0
        var closed = 0

        subscriptions.add(
                Service.call()
                        .getPullRequests(user, repo)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doOnNext { response -> response.forEach {
                            when(it.state) {
                                "open" -> opened ++
                                "closed" -> closed ++
                            }
                        } }
                        .subscribe(
                                { response -> view?.onGetPullRequests(response, opened, closed) },
                                { error -> view?.onError(error) }
                        )
        )
    }
}